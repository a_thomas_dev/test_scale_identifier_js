let intervalsCanvas = document.getElementById('intervalsCanvas');
let ctxIntervalsCanvas = intervalsCanvas.getContext('2d');

let structure = new Image();
let notesEng = new Image();
let notesFr = new Image();
let colorA = new Image();
let colorAsh = new Image();
let colorB = new Image();
let colorC = new Image();
let colorCsh = new Image();
let colorD = new Image();
let colorDsh = new Image();
let colorE = new Image();
let colorF = new Image();
let colorFsh = new Image();
let colorG = new Image();
let colorGsh = new Image();

structure.onload = function() {
  drawStructure();
  drawNotesEng();
}

structure.src = "Images/intervals/structure.svg";
notesEng.src = "Images/intervals/notesEng.svg";
notesFr.src = "Images/intervals/notesFr.svg";
colorA.src = "Images/intervals/colorA.svg";
colorAsh.src = "Images/intervals/colorAsh.svg";
colorB.src = "Images/intervals/colorB.svg";
colorC.src = "Images/intervals/colorC.svg";
colorCsh.src = "Images/intervals/colorCsh.svg";
colorD.src = "Images/intervals/colorD.svg";
colorDsh.src = "Images/intervals/colorDsh.svg";
colorE.src = "Images/intervals/colorE.svg";
colorF.src = "Images/intervals/colorF.svg";
colorFsh.src = "Images/intervals/colorFsh.svg";
colorG.src = "Images/intervals/colorG.svg";
colorGsh.src = "Images/intervals/colorGsh.svg";

function drawStructure() {
  ctxIntervalsCanvas.drawImage(structure, 0, 0);
}

function drawNotesEng() {
  ctxIntervalsCanvas.drawImage(notesEng, 0, 0);
}

function drawNotesFr() {
  ctxIntervalsCanvas.drawImage(notesFr, 0, 0);
}

function intervalColors() {
  intervals.forEach((element, index) => {
    if(element) {
      drawColors(index);
    }
  });
}

function drawColors(index) {
  switch(index) {
    case 0:
      ctxIntervalsCanvas.drawImage(colorA, 0, 0);
      break;
    case 1:
      ctxIntervalsCanvas.drawImage(colorAsh, 0, 0);
      break;
    case 2:
      ctxIntervalsCanvas.drawImage(colorB, 0, 0);
      break;
    case 3:
      ctxIntervalsCanvas.drawImage(colorC, 0, 0);
      break;
    case 4:
      ctxIntervalsCanvas.drawImage(colorCsh, 0, 0);
      break;
    case 5:
      ctxIntervalsCanvas.drawImage(colorD, 0, 0);
      break;
    case 6:
      ctxIntervalsCanvas.drawImage(colorDsh, 0, 0);
      break;
    case 7:
      ctxIntervalsCanvas.drawImage(colorE, 0, 0);
      break;
    case 8:
      ctxIntervalsCanvas.drawImage(colorF, 0, 0);
      break;
    case 9:
      ctxIntervalsCanvas.drawImage(colorFsh, 0, 0);
      break;
    case 10:
      ctxIntervalsCanvas.drawImage(colorG, 0, 0);
      break;
    case 11:
      ctxIntervalsCanvas.drawImage(colorGsh, 0, 0);
      break;
    default:
      console.log('Houston on a un probleme');
  }
}