
let scaleMaj = [1, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1]; //major scale //chgt pour avoir classic scale form at whichscale[0]=0 (inutile (?))
let scaleMelMin = [1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1]; //melodic minor scale
let scalePenta = [1, 0, 1, 0, 0, 1, 0, 1, 1, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 1, 1, 0, 0]; //pentatonic scale (with blue note)                     //TODO faire un visuel spécial pour la blue note
let scaleHarmMaj = [1, 0, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 1, 0, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1]; //harmonic major scale
let scaleHarmMin = [1, 0, 1, 1, 0, 1, 0, 1, 1, 0, 0, 1, 1, 0, 1, 1, 0, 1, 0, 1, 1, 0, 0, 1]; //harmonic minor scale
let scaleAug = [1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1]; //augmented scale
let scaleHalfWholDim = [1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1]; //half-whole diminished scale
let scaleWholeTone = [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0]; //whole tone scale

let scaleMajDiv = document.getElementById('scaleMaj');
let scaleMelMinDiv = document.getElementById('scaleMelMin');
let scalePentaDiv = document.getElementById('scalePenta');
let scaleHarmMajDiv = document.getElementById('scaleHarmMaj');
let scaleHarmMinDiv = document.getElementById('scaleHarmMin');
let scaleAugDiv = document.getElementById('scaleAug');
let scaleHalfWholDimDiv = document.getElementById('scaleHalfWholDim');
let scaleWholeToneDiv = document.getElementById('scaleWholeTone');
// let nbModesForThisScale = 0;
// let thereAreNotesClicked = false;

// let intervalsTest = [0,1,0,1,0,0,0,0,0,0,0,0];
// let intervalsTest = [1,0,1,0,0,0,0,0,0,0,0,0];
// let intervalsTest = [1, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1];

let positionModesScaleMaj = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
let positionModesScaleMelMin = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
let positionModesScalePenta = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
let positionModesScaleHarmMaj = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
let positionModesScaleHarmMin = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
let positionModesScaleAug = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
let positionModesScaleHalfWholDim = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
let positionModesScaleWholeTone = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

function resetPositionModesInAllScales() {
  positionModesScaleMaj = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  positionModesScaleMelMin = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  positionModesScalePenta = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  positionModesScaleHarmMaj = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  positionModesScaleHarmMin = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  positionModesScaleAug = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  positionModesScaleHalfWholDim = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  positionModesScaleWholeTone = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
}


function thereAreNotesClicked(intervals) {
  for (let i = 0; i < intervals.length; i++) {
    if (intervals[i]) {
      return true;
    }
  }

  return false;
}

function nbModesInThatScaleWithTheseIntervals(intervals, scale, positionModesInScale) {
  let nbModesForThisScale = 0;
  positionModesInScale = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

  for (let i = 0; i < intervals.length; i++) {
    if (isMode(intervals, scale, i)) {
      nbModesForThisScale++;
      positionModesInScale[i] = 1;
    }
  }

  //DEBUG
  console.log(positionModesInScale);

  return nbModesForThisScale;
}

function isMode(intervals, scale, index) {
  for (let i = 0; i < intervals.length; i++) {
    if (intervals[i] > 0 && scale[i + index] == 0) {
      return false;
    }
  }

  return true;
}

function fillEachScaleDivsWithNbModesForTheseIntervals(intervals) {
  scaleMajDiv.innerHTML = 'Major scale modes : ' + nbModesInThatScaleWithTheseIntervals(intervals, scaleMaj, positionModesScaleMaj);
  scaleMelMinDiv.innerHTML = 'Melodic minor scale modes : ' + nbModesInThatScaleWithTheseIntervals(intervals, scaleMelMin, positionModesScaleMelMin);
  scalePentaDiv.innerHTML = 'Pentatonic scale modes : ' + nbModesInThatScaleWithTheseIntervals(intervals, scalePenta, positionModesScalePenta);
  scaleHarmMajDiv.innerHTML = 'Harmonic major scale modes : ' + nbModesInThatScaleWithTheseIntervals(intervals, scaleHarmMaj, positionModesScaleHarmMaj);
  scaleHarmMinDiv.innerHTML = 'Harmonic minor scale modes : ' + nbModesInThatScaleWithTheseIntervals(intervals, scaleHarmMin, positionModesScaleHarmMin);
  scaleAugDiv.innerHTML = 'Augmented scale modes : ' + nbModesInThatScaleWithTheseIntervals(intervals, scaleAug, positionModesScaleAug);
  scaleHalfWholDimDiv.innerHTML = 'Half-whole diminished scale modes : ' + nbModesInThatScaleWithTheseIntervals(intervals, scaleHalfWholDim, positionModesScaleHalfWholDim);
  scaleWholeToneDiv.innerHTML = 'Whole tone scale modes : ' + nbModesInThatScaleWithTheseIntervals(intervals, scaleWholeTone, positionModesScaleWholeTone);

  //DEBUG
  console.log('----------');
}

function resetScaleDivs() {
  scaleMajDiv.innerHTML = 'Major scale modes : -';
  scaleMelMinDiv.innerHTML = 'Melodic minor scale modes : -';
  scalePentaDiv.innerHTML = 'Pentatonic scale modes : -';
  scaleHarmMajDiv.innerHTML = 'Harmonic major scale modes : -';
  scaleHarmMinDiv.innerHTML = 'Harmonic minor scale modes : -';
  scaleAugDiv.innerHTML = 'Augmented scale modes : -';
  scaleHalfWholDimDiv.innerHTML = 'Half-whole diminished scale modes : -';
  scaleWholeToneDiv.innerHTML = 'Whole tone scale modes : -';
}

//DEBUG :
// let answer = isMode(intervalsTest, scaleMaj);
// console.log(answer);
// nbModesInThatScaleWithTheseIntervals(intervalsTest, scaleMaj)
// console.log(nbModesForThisScale);