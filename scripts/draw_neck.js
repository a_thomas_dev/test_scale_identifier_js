const guitarCanvas = document.getElementById('guitarNeckCanvas');
const ctxGuitarCanvas = guitarCanvas.getContext('2d');
const imageNeck = new Image();

imageNeck.onload = function () {
  ctxGuitarCanvas.drawImage(imageNeck, 0, 0);
  // resetScaleDivs();
};
imageNeck.src = 'Images/neck.png'; //NB : 1203 * 156px

//position x des frets sur l'imageNeck du manche, de "0" à la 24ème (total : 25) :
const POS_FRET = [41, 131, 214, 292, 365, 434, 499, 559, 617, 671, 722, 770, 815, 857, 898, 935, 971, 1005, 1036, 1066, 1095, 1121, 1147, 1170, 1194];
const Y_FRET0_CORDE1 = 120;
const Y_FRET0_CORDE6 = 33;
const Y_FRET24_CORDE1 = 135;
const Y_FRET24_CORDE6 = 17;
const X_FRET0_TO_FRET24 = 1164; //1195 - 41 = 1164px
const ECART_ENTRE_CORDES_FRET0 = 17.4;
const ECART_ENTRE_CORDES_FRET24 = 23.2;
let notesCorde1 = [];
let notesCorde2 = [];
let notesCorde3 = [];
let notesCorde4 = [];
let notesCorde5 = [];
let notesCorde6 = [];
let dotPosX = [];
let dotPosYCorde1 = [];
let dotPosYCorde2 = [];
let dotPosYCorde3 = [];
let dotPosYCorde4 = [];
let dotPosYCorde5 = [];
let dotPosYCorde6 = [];
const AColor = "#2EAE59";
const AshColor = "#FFF400";  //NB : "#" pose problème : il voit eg A et A# comme la même déclaration
const BColor = "#FF0000";
const CColor = "#8B00DD";
const CshColor = "#FF700A";
const DColor = "#0200DC";
const DshColor = "#16FF00";
const EColor = "#44F4EB";
const FColor = "#FFAB00";
const FshColor = "#FF33D1";
const GColor = "#BA642E";
const GshColor = "#57A8FF";
const A = 1;
const Ash = 2;
const B = 3;
const C = 4;
const Csh = 5;
const D = 6;
const Dsh = 7;
const E = 8;
const F = 9;
const Fsh = 10;
const G = 11;
const Gsh = 12;
// let accordageCorde1 = [E, F, Fsh, G, Gsh, A, Ash, B, C, Csh, D, Dsh];
// let templateNamesCorde = [A, Ash, B, C, Csh, D, Dsh, E, F, Fsh, G, Gsh];
// let templateNamesCorde = [
//   { "note": "A", "color": AColor }, 
//   { "note": "A#", "color": AshColor }, 
//   { "note": "B", "color": BColor }, 
//   { "note": "C", "color": CColor }, 
//   { "note": "C#", "color": CshColor }, 
//   { "note": "D", "color": DColor }, 
//   { "note": "D#", "color": DshColor }, 
//   { "note": "E", "color": EColor }, 
//   { "note": "F", "color": FColor }, 
//   { "note": "F#", "color": FshColor }, 
//   { "note": "G", "color": GColor }, 
//   { "note": "G#", "color": GshColor }];
let templateNamesCorde = [
  { "note": 0, "color": AColor },
  { "note": 1, "color": AshColor },
  { "note": 2, "color": BColor },
  { "note": 3, "color": CColor },
  { "note": 4, "color": CshColor },
  { "note": 5, "color": DColor },
  { "note": 6, "color": DshColor },
  { "note": 7, "color": EColor },
  { "note": 8, "color": FColor },
  { "note": 9, "color": FshColor },
  { "note": 10, "color": GColor },
  { "note": 11, "color": GshColor }];
let accordageCorde1 = templateNamesCorde.slice();
let accordageCorde2 = templateNamesCorde.slice();
let accordageCorde3 = templateNamesCorde.slice();
let accordageCorde4 = templateNamesCorde.slice();
let accordageCorde5 = templateNamesCorde.slice();
let accordageCorde6 = templateNamesCorde.slice();
let intervals = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]


//------------------------------------------------------------------------génération des constantes :
//génération des positions sur l'axe x du dot selon la case jouée :
dotPosX[0] = 4 + (41 - 4) / 2;
for (let i = 0; i < 24; i++) {
  dotPosX[i + 1] = POS_FRET[i] + (POS_FRET[i + 1] - POS_FRET[i]) / 2;
}

//génération des positions sur l'axe y du dot selon la corde jouée :
function populateDotPosY(dotPosYCorde, numeroCorde, dotPosX) {
  for (let i = 0; i < 25; i++) {
    let ecartYEntre2CasesSelonPosx = dotPosX[i] * (Y_FRET24_CORDE1 - Y_FRET0_CORDE1) / X_FRET0_TO_FRET24;
    let ecartYEntre2CordesSelonPosx = dotPosX[i] * (ECART_ENTRE_CORDES_FRET24 - ECART_ENTRE_CORDES_FRET0) / X_FRET0_TO_FRET24;
    let position = Math.round((Y_FRET0_CORDE1 + ecartYEntre2CasesSelonPosx) - (ecartYEntre2CordesSelonPosx + ECART_ENTRE_CORDES_FRET0) * (numeroCorde - 1));
    dotPosYCorde[i] = position;
  }
}

populateDotPosY(dotPosYCorde1, 1, dotPosX);
populateDotPosY(dotPosYCorde2, 2, dotPosX);
populateDotPosY(dotPosYCorde3, 3, dotPosX);
populateDotPosY(dotPosYCorde4, 4, dotPosX);
populateDotPosY(dotPosYCorde5, 5, dotPosX);
populateDotPosY(dotPosYCorde6, 6, dotPosX);
//-------------------------------------------------------------------------------------------------

//pour mettre une note en 1ère place dans le tableau accordageCorde (eg "6" va mettre A en 1ère position) :
function accordageCorde(noteAVide, accordageCorde) {
  for (let i = 0; i < 12; i++) {
    accordageCorde[i].note = templateNamesCorde[i].note;
    accordageCorde[i].color = templateNamesCorde[i].color;
  }
  for (let i = 1; i < noteAVide; i++) {
    accordageCorde.push(accordageCorde.shift());
  }
}
//génération de l'accordage par défaut (EADGBE) :
accordageCorde(E, accordageCorde1);
accordageCorde(A, accordageCorde2);
accordageCorde(D, accordageCorde3);
accordageCorde(G, accordageCorde4);
accordageCorde(B, accordageCorde5);
accordageCorde(E, accordageCorde6);

let chromatismWarningDiv = document.getElementById('chromatismWarning')

//affiche sur quelle case le curseur est quand on clique sur le guitarCanvas :
guitarCanvas.addEventListener('click', function (e) {
  const x = e.clientX - guitarCanvas.offsetLeft;
  const y = e.clientY - guitarCanvas.offsetTop;
  const fret = getFret(x);
  const corde = getCorde(fret, y);
  const MARGE = 8;
  let limiteYHaut = Y_FRET0_CORDE6 - (x * (Y_FRET0_CORDE6 - Y_FRET24_CORDE6) / X_FRET0_TO_FRET24) - MARGE;
  let limiteYBas = Y_FRET0_CORDE1 + (x * (Y_FRET24_CORDE1 - Y_FRET0_CORDE1) / X_FRET0_TO_FRET24) + MARGE;

  //DEBUG...
  // document.getElementById('debug').innerHTML = 'x: ' + x + ', y: ' + y + ', fret: ' + fret + ', corde: ' + corde + ', limiteYHaut: ' + limiteYHaut + ', limiteYBas: ' + limiteYBas;
  // document.getElementById('debug').innerHTML = 'x: ' + x + ', y: ' + y + ', fret: ' + fret + ', corde: ' + corde;

  // console.log(notesCorde1);
  //...DEBUG

  if (y > limiteYHaut && y < limiteYBas) {  //si on a cliqué sur le manche...
    changementNote(fret, corde);
    clearCanvas(ctxGuitarCanvas, 0, 0, guitarCanvas.width, guitarCanvas.height);
    drawNeck();
    drawAllCordesNotes()

    clearCanvas(ctxIntervalsCanvas, 0, 0, intervalsCanvas.width, intervalsCanvas.height);
    intervalColors();
    drawStructure();
    drawNotesEng();
    chromatismWarningDiv.innerHTML = chromatismWarning();
    if (thereAreNotesClicked(intervals)) fillEachScaleDivsWithNbModesForTheseIntervals(intervals);

    //DEBUG :
    // document.getElementById('intervalsTest').innerHTML = intervals[0] + ' ' + intervals[1] + ' ' + intervals[2] + ' ' + intervals[3] + ' ' + intervals[4] + ' ' + intervals[5] + ' ' + intervals[6] + ' ' + intervals[7] + ' ' + intervals[8] + ' ' + intervals[9] + ' ' + intervals[10] + ' ' + intervals[11];
  }
})

function chromatismWarning() {
  if ((intervals[10] && intervals[11] && intervals[0]) || (intervals[11] && intervals[0] && intervals[1])) {
    return 'Chromatism warning !';
  }

  for (let i = 0; i < (intervals.length - 2); i++) {
    if (intervals[i] && intervals[i + 1] && intervals[i + 2]) {
      return 'Chromatism warning !';
    }
  }

  return '';
}

//détermine sur quelle case le curseur est :
function getFret(x) {
  if (x < 41) { return 0; }
  for (let i = 0; i < POS_FRET.length - 1; i++) {
    if (x >= POS_FRET[i] && x < POS_FRET[i + 1]) {
      return i + 1;
    }
  }
}

//déternmine la corde sur laquelle le curseur est :
//TODO : utiliser des const. refaire les coms
function getCorde(fret, y) {
  for (j = 0; j < 6; j++)   //on test pr voir sur quelle corde est le curseur
  {
    let debutBoxMiA = 24.3 - (24.3 - 6.4) / 24 * (fret);   //pos.y du début 1er rectangle @case 1 = 24,3 ; @case 24 = 6.4 (noté à la main) :
    //écart entre deux corde (=hauteur box) @case1=(120-33)/5=17.4 ; @case24=(134-18/5)=23.2
    //=> déduction pour chaque case car linéaire : déviation = case*(23.2-17.4)/24
    let distanceEntre2Cordes = 17.4 + (fret) * (23.2 - 17.4) / 24;     //distance entre deux cordes selon la case (déduite plus haut)
    if (y >= debutBoxMiA + j * distanceEntre2Cordes && y < debutBoxMiA + (j + 1) * distanceEntre2Cordes) {
      let pourInverser = [6, 5, 4, 3, 2, 1];
      return pourInverser[j];
    }
  }
}

function addOrSubstractNoteFromInterval(accordageCorde, fret, statusNote) {
  fret = fret >= 12 ? fret - 12 : fret;

  if (statusNote) {
    intervals[accordageCorde[fret].note]++;
  } else {
    intervals[accordageCorde[fret].note]--;
  }
}

//gère l'ajout ou la suppression d'une note selon son existence d'avant clic
function changementNote(fret, corde) {
  switch (corde) {
    case 1:
      statusNote = insertOrRemoveNoteOnCorde(notesCorde1, fret);
      addOrSubstractNoteFromInterval(accordageCorde1, fret, statusNote);
      break;
    case 2:
      statusNote = insertOrRemoveNoteOnCorde(notesCorde2, fret);
      addOrSubstractNoteFromInterval(accordageCorde2, fret, statusNote);
      break;
    case 3:
      statusNote = insertOrRemoveNoteOnCorde(notesCorde3, fret);
      addOrSubstractNoteFromInterval(accordageCorde3, fret, statusNote);
      break;
    case 4:
      statusNote = insertOrRemoveNoteOnCorde(notesCorde4, fret);
      addOrSubstractNoteFromInterval(accordageCorde4, fret, statusNote);
      break;
    case 5:
      statusNote = insertOrRemoveNoteOnCorde(notesCorde5, fret);
      addOrSubstractNoteFromInterval(accordageCorde5, fret, statusNote);
      break;
    case 6:
      statusNote = insertOrRemoveNoteOnCorde(notesCorde6, fret);
      addOrSubstractNoteFromInterval(accordageCorde6, fret, statusNote);
      break;
    default:
      console.log('Houston on a un probleme');
  }
}

function insertOrRemoveNoteOnCorde(notesCorde, fret) {
  notesCorde[fret] = notesCorde[fret] == true ? false : true;

  return notesCorde[fret];
}

function drawAllCordesNotes() {
  drawCordeNotes(notesCorde1, 1, accordageCorde1);
  drawCordeNotes(notesCorde2, 2, accordageCorde2);
  drawCordeNotes(notesCorde3, 3, accordageCorde3);
  drawCordeNotes(notesCorde4, 4, accordageCorde4);
  drawCordeNotes(notesCorde5, 5, accordageCorde5);
  drawCordeNotes(notesCorde6, 6, accordageCorde6);
}

//affiche les notes d'une corde sur le manche
function drawCordeNotes(notesCorde, corde, accordageCorde) {
  notesCorde.forEach((element, index) => {
    if (element) {
      let indexColor = index >= 12 ? index - 12 : index;
      let color = accordageCorde[indexColor].color;
      drawCircle(index, corde, 8, color);
    }
  });
}

//affiche la note choisie sur le manche par un rond rempli d'une couleur
function drawCircle(fret, corde, radius, color) {
  let x = dotPosX[fret];
  let y;

  switch (corde) {
    case 1:
      y = dotPosYCorde1[fret];
      break;
    case 2:
      y = dotPosYCorde2[fret];
      break;
    case 3:
      y = dotPosYCorde3[fret];
      break;
    case 4:
      y = dotPosYCorde4[fret];
      break;
    case 5:
      y = dotPosYCorde5[fret];
      break;
    case 6:
      y = dotPosYCorde6[fret];
      break;
    default:
      console.log('Houston on a un probleme');
  }

  ctxGuitarCanvas.beginPath();
  ctxGuitarCanvas.arc(x, y, radius, 0, 2 * Math.PI);
  ctxGuitarCanvas.fillStyle = color;
  ctxGuitarCanvas.fill();
}

//TODO : copier la doc pour peupler les autres f°
/**
 * Efface une région rectangulaire spécifiée du guitarCanvas.
 * @param {number} x - La coordonnée x du coin supérieur gauche de la région à effacer
 * @param {number} y - La coordonnée y du coin supérieur gauche de la région à effacer
 * @param {number} width - La largeur de la région à effacer
 * @param {number} height - La hauteur de la région à effacer
 */

function clearCanvas(ctxCanvas, x, y, width, height) {
  ctxCanvas.clearRect(x, y, width, height);
}

function drawNeck() {
  ctxGuitarCanvas.drawImage(imageNeck, 0, 0);
}

//fonction qui vide les arrays notesCorde quand on clic sur le bouton reset :
function btnReset() {
  notesCorde1 = [];
  notesCorde2 = [];
  notesCorde3 = [];
  notesCorde4 = [];
  notesCorde5 = [];
  notesCorde6 = [];
  intervals = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  clearCanvas(ctxGuitarCanvas, 0, 0, guitarCanvas.width, guitarCanvas.height);
  drawNeck();
  clearCanvas(ctxIntervalsCanvas, 0, 0, intervalsCanvas.width, intervalsCanvas.height);
  drawStructure();
  drawNotesEng();
  resetScaleDivs();
  resetPositionModesInAllScales();
}



//------------------------------------------------------------------------DEBUG :
//affichage de la position x et y du curseur sur le guitarCanvas :
guitarCanvas.addEventListener('mousemove', function (e) {
  const x = e.clientX - guitarCanvas.offsetLeft;
  const y = e.clientY - guitarCanvas.offsetTop;
  // document.getElementById('debug').innerHTML = 'x: ' + x + ', y: ' + y;
})


//------------------------------------------------------------------------todolist :
//-react/angular : pouvoir sauvegarder des pages, avoir des tabs pour switcher ?
//choisir l'accordage


//------------------------------------------------------------------------VRAC gardé au cas ou :

// let dotPosY = [];
// //let's populate dotPosY :  //NB : posY corde MiG milieu 1ère case = 120
// for (i = 0 ; i < 6 ; i++){
//   for (j = 0 ; j < 24 ; j++)
//   {
//       let ecartEntre2Cases = dotPosX[j] * (135-120) / 1203;      //120 + ecartEntre2Cases = corde de MiG, pour avoir une réf (120 = posY@case 1 (corde MiG), 135@case 24)
//       let ecartEntre2Cordes = dotPosX[j] * (23.2-17.4) / 1203;    //17.4 : écart entre deux cordes @case 1, 23.2 : @case 24
//       let position = Math.round ((120 + ecartEntre2Cases) - (ecartEntre2Cordes + 17.4) * i);
//       dotPosY[j+i*24] = position;
//   }
// }
// for (i = 144 ; i < 150 ; i++) { //pour les 6 cordes à vide. NB : 87/5=17.4
//       let position = 120 - 17.4 * (i - 144);
//       dotPosY[i] = Math.round(position);
// }
// // console.log(dotPosY);