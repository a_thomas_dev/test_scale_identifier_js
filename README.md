# Scale Identifier JavaScript
![JavaScript](https://img.shields.io/badge/JavaScript-white?style=plastic&logo=javascript&logoColor=white&color=%23F7DF1E)
![HTML](https://img.shields.io/badge/HTML-white?style=plastic&logo=html5&logoColor=white&color=%23E34F26)
![CSS](https://img.shields.io/badge/CSS-white?style=plastic&logo=css3&logoColor=white&color=%231572B6)

## Présentation
Un prototype d'identificateur de gammes pour les guitaristes.